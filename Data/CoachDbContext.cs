﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;
using AtheteExampleM2M.Models;
using Microsoft.EntityFrameworkCore;

namespace AtheteExampleM2M.Data
{
    public class CoachDbContext : DbContext
    {
        // Tables
        public DbSet<Coach> Coaches { get; set; }
        public DbSet<Certification> Certifications { get; set; }

        public CoachDbContext([NotNull] DbContextOptions options) : base(options)
        {
        }
    }
}
