﻿using System;
using System.Collections.Generic;
using AutoMapper;
using System.Linq;
using System.Threading.Tasks;
using AtheteExampleM2M.DTOs;
using AtheteExampleM2M.Models;

namespace AtheteExampleM2M.Profiles
{
    public class CoachProfile : Profile
    {
        public CoachProfile()
        {
            CreateMap<Coach, CoachReadDTO>()
                .ForMember(cdto => cdto.Certifications, opt => opt
                    .MapFrom(c => c.Certifications.Select(c => c.Id).ToArray()));
        }
    }
}
