﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AtheteExampleM2M.Data;
using AtheteExampleM2M.DTOs;
using AtheteExampleM2M.Models;
using AutoMapper;
using Microsoft.AspNetCore.Razor.Language.Extensions;

namespace AtheteExampleM2M.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CoachesController : ControllerBase
    {
        private readonly CoachDbContext _context;

        // Add automapper via DI
        private readonly IMapper _mapper;

        public CoachesController(CoachDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Coaches
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CoachReadDTO>>> GetCoaches()
        {
            return _mapper.Map<List<CoachReadDTO>>(await _context.Coaches
                .Include(c => c.Certifications)
                .ToListAsync());
        }

        // GET: api/Coaches/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Coach>> GetCoach(int id)
        {
            var coach = await _context.Coaches.FindAsync(id);

            if (coach == null)
            {
                return NotFound();
            }

            return coach;
        }

        // PUT: api/Coaches/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCoach(int id, Coach coach)
        {
            if (id != coach.Id)
            {
                return BadRequest();
            }

            _context.Entry(coach).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CoachExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Coaches
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Coach>> PostCoach(Coach coach)
        {
            _context.Coaches.Add(coach);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCoach", new { id = coach.Id }, coach);
        }

        // DELETE: api/Coaches/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCoach(int id)
        {
            var coach = await _context.Coaches.FindAsync(id);
            if (coach == null)
            {
                return NotFound();
            }

            _context.Coaches.Remove(coach);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool CoachExists(int id)
        {
            return _context.Coaches.Any(e => e.Id == id);
        }

        // Assigning certification to a coach

        // This end point will be used to assign certificates/certifications to a coach 
        [HttpPut("{id}/certificates")]
        public async Task<IActionResult> UpdateCoachCertificates(int id, List<int> certificates)
        {
            if (!CoachExists(id))
            {
                return NotFound();
            }

            // May want to place this in a service - controller is getting bloated
            Coach coachToUpdateCerts = await _context.Coaches
                .Include(c => c.Certifications)
                .Where(c => c.Id == id)
                .FirstAsync();

            // Loop through certificates, try and assign to coach
            List<Certification> certs = new();
            foreach (int certId in certificates)
            {
                Certification cert = await _context.Certifications.FindAsync(certId);
                if (cert == null)
                    return BadRequest("Certification doesnt exist!");
                certs.Add(cert);
            }

            coachToUpdateCerts.Certifications = certs;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return NoContent();
        }
    }
}
